package main

import (
	"log"
	"net/http"
	"time"

	"golang.org/x/sync/errgroup"

	"seele-monitor-api/core"
	"seele-monitor-api/ws"
)

var (
	g errgroup.Group
)

func main() {

	handlerConfig := &core.EngineConfig{
		DisableConsoleColor: false,
		WriteLog:            true,
		LogFile:             "seele-monitor-api.logs",
		LimitConnections:    0,
	}

	server := core.SlServer{
		Server: &http.Server{
			Addr:    ":9999",
			Handler: handlerConfig.Init(),
			//ReadTimeout:    60 * time.Second,
			WriteTimeout:   30 * time.Second,
			IdleTimeout:    time.Second * 10,
			MaxHeaderBytes: 1 << 20,
		},
		G: &g,
	}

	server.RunServer()


	// web socket send data
	time.Sleep(5 * time.Second)
	ws.Init()
	go ws.NewClient()

	if err := g.Wait(); err != nil {
		log.Fatal(err)
	}



}
