package ws

import (
	"testing"
)

func TestNewClient(t *testing.T) {
	Init()
	done := make(chan bool)
	go NewClient()

	<-done
}
