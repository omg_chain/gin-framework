package core

import (
	"io"
	"net/http"
	"os"

	"github.com/aviddiviner/gin-limit"
	"github.com/gin-gonic/gin"

	"seele-monitor-api/api/routers"
)

type EngineConfig struct {
	DisableConsoleColor bool // disable the console color
	WriteLog            bool
	LogFile             string
	LimitConnections    int
	Routers             []gin.IRoutes
}

const (
	defaultLogFile = "seele-monitor.logs"
)

func (config *EngineConfig) initEngineConfig() *gin.Engine {
	if config == nil {
		panic("engine config-watcher should not be nil")
	}

	if config.DisableConsoleColor {
		gin.DisableConsoleColor()
	}

	if config.WriteLog {
		gin.DisableConsoleColor()

		logFile := config.LogFile
		if logFile == "" {
			logFile = defaultLogFile
		}
		f, _ := os.Create(defaultLogFile)
		gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
	} else {
		gin.DefaultWriter = os.Stdout
	}

	e := gin.New()

	// use logs middleware
	e.Use(gin.Logger())
	e.Use(gin.Recovery())

	// By default, http.ListenAndServe (which gin.Run wraps) will serve an unbounded number of requests.
	// Limiting the number of simultaneous connections can sometimes greatly speed things up under load
	if config.LimitConnections > 0 {
		e.Use(limit.MaxAllowed(config.LimitConnections))
	}

	return e
}

func (config *EngineConfig) Init() http.Handler {
	e := config.initEngineConfig()

	// todo: here init the routers, need refactor
	routers.InitRouters(e)
	return e
}
