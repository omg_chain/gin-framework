## Seele Monitor API

### Env
>recommend
```
go 1.10+
```

### Project structure
```
├── api: api interface
│   ├── filters:  request filter
│   ├── handlers: router handler
│   └── routers:  the http router
├── conf: config files
├── core
│   ├── config: configure
│   ├── logs: third logger
│   └── utils: utils
├── json_rpc: json rpc
├── tools:  build script
│   └── prebuild
│       └── shell
├── vendor: third dependencies
└── ws: web socket

```
