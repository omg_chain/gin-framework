package routers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

func InitWsRouters(e *gin.Engine) {
	e.GET("/ws", bindWsHandler())
}

func bindWsHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		wsHandler(c.Writer, c.Request)
	}
}

func wsHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("Failed to set websocket upgrade: %+v", err)
		return
	}
	for {
		msgType, msgData, err := conn.ReadMessage()
		switch err.(type) {
		case *websocket.CloseError:
			log.Printf("websocket is closed, error is %v", err)
			return
		default:
			log.Printf("websocket error is %v", err)
			return
		}
		if err != nil {
			break
		}

		// Skip binary messages
		if msgType != websocket.TextMessage {
			continue
		}

		path := r.URL.Path
		log.Printf("WsHandler path is %v", path)
		//todo do something
		var requestData map[string]interface{}
		err = json.Unmarshal(msgData, &requestData)
		if err != nil {
			log.Printf("WsHandler receive msg %v", string(msgData))
		} else {
			log.Printf("WsHandler receive msg is %+v", requestData)
		}

		// write
		conn.WriteMessage(msgType, msgData)
		log.Printf("output message, type(1=text, 2=binary): %+v, msg: %+v\n", msgType, string(msgData))
	}
}
