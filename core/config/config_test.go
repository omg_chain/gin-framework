package config

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewConfig(t *testing.T) {
	config, err := NewConfig("ini", "../../conf/gin.conf")
	if err != nil {
		//assert.Errorf(t, err, "error occur %v",  err)
		fmt.Errorf("%v", err)
	}
	fmt.Printf("config is %v\n", config)
	appName := config.String("app_name")
	fmt.Printf("appName is: %v\n", appName)

}

func TestRegister(t *testing.T) {
	adapterAll := adapters
	fmt.Printf("adapterAll is: %v\n", adapterAll)
	fmt.Printf("adapter ini is: %v\n", adapterAll["ini"])
	fmt.Printf("adapter json is: %v\n", adapterAll["json"])

}

func TestExpandValueEnv(t *testing.T) {
	goPath := ExpandValueEnv("${GOPATH}")
	assert.NotEmptyf(t, goPath, "go path is: %v", goPath)

}

func TestExpandValueEnvBad(t *testing.T) {
	goPath := ExpandValueEnv("${GOPATH}")
	assert.Emptyf(t, goPath, "go path is: %v", goPath)

}
