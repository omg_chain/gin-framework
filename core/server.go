package core

import (
	"net/http"

	"github.com/fvbock/endless"
	"golang.org/x/sync/errgroup"
	"time"
)

const (
	defaultHammerTime = 10
)

type SlServer struct {
	Server   *http.Server
	CertFile string
	KeyFile  string
	G        *errgroup.Group
}

func (sl *SlServer) NewServer(server *http.Server, g *errgroup.Group) {
	sl.Server = server
	sl.G = g
}

func (sl *SlServer) NewServerTls(server *http.Server, certFile string, keyFile string, g *errgroup.Group) {
	sl.Server = server
	sl.CertFile = certFile
	sl.KeyFile = keyFile
	sl.G = g
}

// Run our server in a goroutine so that it doesn't block.
func (sl *SlServer) RunServer() {
	sl.G.Go(func() error {
		//	return server.ListenAndServe()
		// Graceful restart or stop we use fvbock/endless to replace the default ListenAndServe
		endless.DefaultHammerTime = defaultHammerTime * time.Second
		return endless.ListenAndServe(sl.Server.Addr, sl.Server.Handler)
	})
}

func (sl *SlServer) RunServerTls() {
	sl.G.Go(func() error {
		//return server.ListenAndServeTLS(certFile, keyFile)
		// Graceful restart or stop we use fvbock/endless to replace the default ListenAndServeTLS
		endless.DefaultHammerTime = defaultHammerTime * time.Second
		return endless.ListenAndServeTLS(sl.Server.Addr, sl.CertFile, sl.KeyFile, sl.Server.Handler)
	})
}
