package core

const (
	// VERSION represent seele monitor version.
	VERSION = "0.0.1"

	// DEV is for develop
	DEV = "dev"
	// RELEASE is for production
	RELEASE = "release"
)
