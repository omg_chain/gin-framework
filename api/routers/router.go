package routers

import (
	"github.com/gin-gonic/gin"

	"seele-monitor-api/api"
	"seele-monitor-api/api/filters"
	"seele-monitor-api/api/handlers"
)

func InitRouters(e *gin.Engine) {
	v1 := e.Group("/api/v1")
	v1.Use(filters.BaseFilter())
	{
		v1.GET("/ping", handlers.Ping())
		//engine.GET("/ping", logsHandler, handlers.Ping())
		v1.GET("/pong", handlers.Pong())
		v1.POST("/pong", handlers.Kong())
		v1.GET("/long_async", handlers.LongAsync())
	}

	v2 := SlRouter{
		e,
	}
	v2.GET("/v2/ping", handlers.Ping())
	v2.AddToGroup(api.HTTP_POST, "/v2", "/pong", handlers.Pong())
	v2.Add(api.HTTP_POST, "/v2", handlers.Ping())
	v2.AddToGroup(api.HTTP_POST, "/v2", "/ok", handlers.Ping())

	//web socket
	InitWsRouters(e)
	//go h.run()
	//e.GET("/ws", func(c *gin.Context) {
	//	wsHandler(c.Writer, c.Request)
	//})

}
