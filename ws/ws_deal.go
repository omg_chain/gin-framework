package ws

import (
	"log"

	rpc "seele-monitor-api/json_rpc"
)

var rpcUrl = "http://127.0.0.1:8545"

func writeMessage() (data string, err error) {
	client := rpc.New(rpcUrl)
	web3ClientVersion, err := client.GoodWeb3ClientVersion()
	if err != nil {
		log.Fatal(err)
		return "", err
	}
	log.Printf("Web3ClientVersion is %v", web3ClientVersion)
	return web3ClientVersion, nil
}

func getMetrics() (data string, err error) {
	client := rpc.New(rpcUrl)
	web3ClientVersion, err := client.GoodWeb3ClientVersion()
	if err != nil {
		log.Fatal(err)
		return "", err
	}
	log.Printf("Web3ClientVersion is %v", web3ClientVersion)
	return web3ClientVersion, nil
}
