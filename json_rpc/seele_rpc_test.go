package json_rpc

import (
	"errors"
	"fmt"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/suite"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"net/http"
	"testing"
)

type SlRPCTestSuite struct {
	suite.Suite
	rpc *SlRPC
}

func (s *SlRPCTestSuite) registerResponse(result string, callback func([]byte)) {
	httpmock.Reset()
	response := fmt.Sprintf(`{"jsonrpc":"2.0", "id":1, "result": %s}`, result)
	httpmock.RegisterResponder("POST", s.rpc.url, func(request *http.Request) (*http.Response, error) {
		callback(s.getBody(request))
		return httpmock.NewStringResponse(200, response), nil
	})
}

func (s *SlRPCTestSuite) registerResponseError(err error) {
	httpmock.Reset()
	httpmock.RegisterResponder("POST", s.rpc.url, func(request *http.Request) (*http.Response, error) {
		return nil, err
	})
}

func (s *SlRPCTestSuite) getBody(request *http.Request) []byte {
	defer request.Body.Close()
	body, err := ioutil.ReadAll(request.Body)
	s.Require().Nil(err)

	return body
}

func (s *SlRPCTestSuite) methodEqual(body []byte, expected string) {
	value := gjson.GetBytes(body, "method").String()

	s.Require().Equal(expected, value)
}

func (s *SlRPCTestSuite) paramsEqual(body []byte, expected string) {
	value := gjson.GetBytes(body, "params").Raw
	if expected == "null" {
		s.Require().Equal(expected, value)
	} else {
		s.JSONEq(expected, value)
	}
}

func (s *SlRPCTestSuite) SetupSuite() {
	s.rpc = NewSlRPC("http://127.0.0.1:8545", WithHttpClient(http.DefaultClient), WithLogger(nil), WithDebug(false))

	httpmock.Activate()
}

func (s *SlRPCTestSuite) TearDownSuite() {
	httpmock.Deactivate()
}

func (s *SlRPCTestSuite) TearDownTest() {
	httpmock.Reset()
}

func (s *SlRPCTestSuite) TestWeb3ClientVersion() {
	response := `{"jsonrpc":"2.0", "id":1, "result": "test client"}`

	httpmock.RegisterResponder("POST", s.rpc.url, func(request *http.Request) (*http.Response, error) {
		body := s.getBody(request)
		s.methodEqual(body, "web3.clientVersion")
		s.paramsEqual(body, `null`)

		return httpmock.NewStringResponse(200, response), nil
	})

	v, err := s.rpc.Web3ClientVersion()
	s.Require().Nil(err)
	s.Require().Equal("test client", v)
}

func (s *SlRPCTestSuite) TestWeb3Sha3() {
	response := `{"jsonrpc":"2.0", "id":1, "result": "sha3result"}`

	httpmock.RegisterResponder("POST", s.rpc.url, func(request *http.Request) (*http.Response, error) {
		body := s.getBody(request)
		s.methodEqual(body, "web3.sha3")
		s.paramsEqual(body, `["0x64617461"]`)

		return httpmock.NewStringResponse(200, response), nil
	})

	result, err := s.rpc.Web3Sha3([]byte("data"))
	s.Require().Nil(err)
	s.Require().Equal("sha3result", result)
}

func (s *SlRPCTestSuite) TestNetVersion() {
	response := `{"jsonrpc":"2.0", "id":1, "result": "v2b3"}`

	httpmock.RegisterResponder("POST", s.rpc.url, func(request *http.Request) (*http.Response, error) {
		body := s.getBody(request)
		s.methodEqual(body, "net.version")
		s.paramsEqual(body, "null")

		return httpmock.NewStringResponse(200, response), nil
	})

	v, err := s.rpc.NetVersion()
	s.Require().Nil(err)
	s.Require().Equal("v2b3", v)
}

func (s *SlRPCTestSuite) TestNetPeerCount() {
	// Test error
	s.registerResponseError(errors.New("Error"))
	peerCount, err := s.rpc.NetPeerCount()
	s.Require().NotNil(err)
	s.Require().Equal(0, peerCount)

	// Test success
	s.registerResponse(`"0x22"`, func(body []byte) {
		s.methodEqual(body, "net.peerCount")
		s.paramsEqual(body, "null")
	})

	peerCount, err = s.rpc.NetPeerCount()
	s.Require().Nil(err)
	s.Require().Equal(34, peerCount)
}

func (s *SlRPCTestSuite) TestSlProtocolVersion() {
	s.registerResponse(`"54"`, func(body []byte) {
		s.methodEqual(body, "sl.protocolVersion")
		s.paramsEqual(body, "null")
	})

	protocolVersion, err := s.rpc.SlProtocolVersion()
	s.Require().Nil(err)
	s.Require().Equal("54", protocolVersion)
}

// test suite run
func TestSlRPCTestSuite(t *testing.T) {
	suite.Run(t, new(SlRPCTestSuite))
}
