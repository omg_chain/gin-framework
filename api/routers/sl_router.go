package routers

import (
	"github.com/gin-gonic/gin"
)

type SlRouter struct {
	Router *gin.Engine
}

func (r *SlRouter) Add(httpMethod, relativePath string, handlers ...gin.HandlerFunc) {
	r.Router.Handle(httpMethod, relativePath, handlers...)
}

func (r *SlRouter) AddToGroup(httpMethod, group string, relativePath string, handlers ...gin.HandlerFunc) {
	if group == "" {
		r.Router.Handle(httpMethod, relativePath, handlers...)
		return
	}
	r.Router.Group(group).Handle(httpMethod, relativePath, handlers...)
}

// POST is a shortcut for router.Handle("POST", path, handle).
func (r *SlRouter) POST(relativePath string, handlers ...gin.HandlerFunc) {
	r.Router.POST(relativePath, handlers...)
}

// GET is a shortcut for router.Handle("GET", path, handle).
func (r *SlRouter) GET(relativePath string, handlers ...gin.HandlerFunc) {
	r.Router.GET(relativePath, handlers...)
}

// DELETE is a shortcut for router.Handle("DELETE", path, handle).
func (r *SlRouter) DELETE(relativePath string, handlers ...gin.HandlerFunc) {
	r.Router.DELETE(relativePath, handlers...)
}

// PATCH is a shortcut for router.Handle("PATCH", path, handle).
func (r *SlRouter) PATCH(relativePath string, handlers ...gin.HandlerFunc) {
	r.Router.PATCH(relativePath, handlers...)
}

// PUT is a shortcut for router.Handle("PUT", path, handle).
func (r *SlRouter) PUT(relativePath string, handlers ...gin.HandlerFunc) {
	r.Router.PUT(relativePath, handlers...)
}

// OPTIONS is a shortcut for router.Handle("OPTIONS", path, handle).
func (r *SlRouter) OPTIONS(relativePath string, handlers ...gin.HandlerFunc) {
	r.Router.OPTIONS(relativePath, handlers...)
}

// HEAD is a shortcut for router.Handle("HEAD", path, handle).
func (r *SlRouter) HEAD(relativePath string, handlers ...gin.HandlerFunc) {
	r.Router.HEAD(relativePath, handlers...)
}
