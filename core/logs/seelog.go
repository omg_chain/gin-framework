package logs

import (
	"fmt"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

var Logs *log.Logger

func init() {
	// Log as JSON instead of the default ASCII formatter.
	//logs.SetFormatter(&logs.JSONFormatter{})
	log.SetFormatter(&log.TextFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	gin.Logger()

	// Only logs the warning severity or above.
	log.SetLevel(log.DebugLevel)

}

func formatLog(f interface{}, v ...interface{}) string {
	var msg string
	switch f.(type) {
	case string:
		msg = f.(string)
		if len(v) == 0 {
			return msg
		}
		if strings.Contains(msg, "%") && !strings.Contains(msg, "%%") {
			//format string
		} else {
			//do not contain format char
			msg += strings.Repeat(" %v", len(v))
		}
	default:
		msg = fmt.Sprint(f)
		if len(v) == 0 {
			return msg
		}
		msg += strings.Repeat(" %v", len(v))
	}
	return fmt.Sprintf(msg, v...)
}

func Debug(f interface{}, args ...interface{}) {
	log.Debug(formatLog(f, args...))
}

func Info(f interface{}, args ...interface{}) {
	log.Info(formatLog(f, args...))
}

func Warn(f interface{}, args ...interface{}) {
	log.Warn(formatLog(f, args...))
}

func Printf(f interface{}, args ...interface{}) {
	log.Print(formatLog(f, args...))
}

func Panic(f interface{}, args ...interface{}) {
	log.Panic(formatLog(f, args...))
}

func Error(f interface{}, args ...interface{}) {
	log.Error(formatLog(f, args...))
}

//

func Debugln(v ...interface{}) {
	log.Debugln(v...)
}

func Infoln(args ...interface{}) {
	log.Infoln(args...)
}

func Warnln(args ...interface{}) {
	log.Warnln(args...)
}

func Printfln(args ...interface{}) {
	log.Println(args...)
}

func Panicln(args ...interface{}) {
	log.Panicln(args...)
}

func Errorln(args ...interface{}) {
	log.Errorln(args...)
}
