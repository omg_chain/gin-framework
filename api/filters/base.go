package filters

import (
	"github.com/gin-gonic/gin"

	"seele-monitor-api/core/logs"
)

func BaseFilter() gin.HandlerFunc {
	return func(c *gin.Context) {
		contentType := c.ContentType()
		logs.Info("contentType: %v", contentType)
		if contentType != gin.MIMEJSON {
			logs.Error("contentType must be %v", gin.MIMEJSON)
			c.Abort()
		}
		c.Next()
	}
}
