#!/bin/bash

# --------------------------------------------------
current_path=`pwd`
#默认的编译输出路径及目标文件名称
#target="./output/bin/seele-monitor-api"
target="./bin/seele-monitor-api"

# --------------------------------------------------
#get govendor
echo "get govendor"
cd $current_path
go get github.com/kardianos/govendor

# --------------------------------------------------
#build
echo "build"
if [ $@ = "debug" ]
then
    go build -o $target && echo "ok"
else
    go build -ldflags "-s -w" -o $target && echo "ok"
fi
mkdir -p ./output/bin
cp -rf $target ./output/bin/seele-monitor-api 2>/dev/null
# --------------------------------------------------
