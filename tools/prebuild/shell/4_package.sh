#!/bin/bash

# "----------------------------------------------"
current_path=`pwd`
#默认的打包名称
package_name="seele-monitor-api-package"
package_file="$package_name.tar.gz"
# "----------------------------------------------"

# "----------------------------------------------"
cd $current_path
if [ -d "$package_file" ]
then
    rm -rf "$package_file"
fi
mkdir "$package_name"
mkdir "$package_name/log"
mkdir "$package_name/data"
cp -R "bin" "$package_name"
cp -R "conf" "$package_name"

cd $current_path
if [ -f $package_file ]
then
    rm -f $package_file
fi
tar -zcvf $package_file "$package_name"
rm -rf "$package_name"
# "----------------------------------------------"

# "----------------------------------------------"
echo "ok"
# "----------------------------------------------"
