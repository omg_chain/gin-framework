package json_rpc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/big"
)

// good interface  fot test
func (rpc *SlRPC) GoodWeb3ClientVersion() (string, error) {
	var clientVersion string
	err := rpc.call("web3_clientVersion", &clientVersion)
	return clientVersion, err
}

// Web3ClientVersion returns the current client version.
func (rpc *SlRPC) Web3ClientVersion() (string, error) {
	var clientVersion string

	err := rpc.call("web3.clientVersion", &clientVersion)
	return clientVersion, err
}

// Web3Sha3 returns Keccak-256 (not the standardized SHA3-256) of the given data.
func (rpc *SlRPC) Web3Sha3(data []byte) (string, error) {
	var hash string

	err := rpc.call("web3.sha3", &hash, fmt.Sprintf("0x%x", data))
	return hash, err
}

// NetVersion returns the current network protocol version.
func (rpc *SlRPC) NetVersion() (string, error) {
	var version string

	err := rpc.call("net.version", &version)
	return version, err
}

// NetListening returns true if client is actively listening for network connections.
func (rpc *SlRPC) NetListening() (bool, error) {
	var listening bool

	err := rpc.call("net.listening", &listening)
	return listening, err
}

// NetPeerCount returns number of peers currently connected to the client.
func (rpc *SlRPC) NetPeerCount() (int, error) {
	var response string
	if err := rpc.call("net.peerCount", &response); err != nil {
		return 0, err
	}

	return ParseInt(response)
}

// SlProtocolVersion returns the current sm protocol version.
func (rpc *SlRPC) SlProtocolVersion() (string, error) {
	var protocolVersion string

	err := rpc.call("sl.protocolVersion", &protocolVersion)
	return protocolVersion, err
}

// SlSyncing returns an object with data about the sync status or false.
func (rpc *SlRPC) EthSyncing() (*Syncing, error) {
	result, err := rpc.RawCall("sl.syncing")
	if err != nil {
		return nil, err
	}
	syncing := new(Syncing)
	if bytes.Equal(result, []byte("false")) {
		return syncing, nil
	}
	err = json.Unmarshal(result, syncing)
	return syncing, err
}

// SlAccounts returns a list of addresses owned by client.
func (rpc *SlRPC) SlAccounts() ([]string, error) {
	accounts := []string{}

	err := rpc.call("sl.accounts", &accounts)
	return accounts, err
}

// SlBlockNumber returns the number of most recent block.
func (rpc *SlRPC) SlBlockNumber() (int, error) {
	var response string
	if err := rpc.call("sl.blockNumber", &response); err != nil {
		return 0, err
	}

	return ParseInt(response)
}

// SlGetBalance returns the balance of the account of given address in wei.
func (rpc *SlRPC) SlGetBalance(address, block string) (big.Int, error) {
	var response string
	if err := rpc.call("sl.getBalance", &response, address, block); err != nil {
		return big.Int{}, err
	}

	return ParseBigInt(response)
}

// SlGetStorageAt returns the value from a storage position at a given address.
func (rpc *SlRPC) SlGetStorageAt(data string, position int, tag string) (string, error) {
	var result string

	err := rpc.call("sl.getStorageAt", &result, data, IntToHex(position), tag)
	return result, err
}

// SlGetTransactionCount returns the number of transactions sent from an address.
func (rpc *SlRPC) SlGetTransactionCount(address, block string) (int, error) {
	var response string

	if err := rpc.call("sl.getTransactionCount", &response, address, block); err != nil {
		return 0, err
	}

	return ParseInt(response)
}

// SlGetBlockTransactionCountByHash returns the number of transactions in a block from a block matching the given block hash.
func (rpc *SlRPC) SlGetBlockTransactionCountByHash(hash string) (int, error) {
	var response string

	if err := rpc.call("sl.getBlockTransactionCountByHash", &response, hash); err != nil {
		return 0, err
	}

	return ParseInt(response)
}

// SlGetBlockTransactionCountByNumber returns the number of transactions in a block from a block matching the given block
func (rpc *SlRPC) SlGetBlockTransactionCountByNumber(number int) (int, error) {
	var response string

	if err := rpc.call("sl.getBlockTransactionCountByNumber", &response, IntToHex(number)); err != nil {
		return 0, err
	}

	return ParseInt(response)
}

// SlGetCode returns code at a given address.
func (rpc *SlRPC) SlGetCode(address, block string) (string, error) {
	var code string

	err := rpc.call("sl.getCode", &code, address, block)
	return code, err
}

// SlSign signs data with a given address.
func (rpc *SlRPC) SlSign(address, data string) (string, error) {
	var signature string

	err := rpc.call("sl.sign", &signature, address, data)
	return signature, err
}

func (rpc *SlRPC) getBlock(method string, withTransactions bool, params ...interface{}) (*Block, error) {
	var response proxyBlock
	if withTransactions {
		response = new(proxyBlockWithTransactions)
	} else {
		response = new(proxyBlockWithoutTransactions)
	}

	err := rpc.call(method, response, params...)
	if err != nil {
		return nil, err
	}
	block := response.toBlock()

	return &block, nil
}

// SlGetBlockByHash returns information about a block by hash.
func (rpc *SlRPC) SlGetBlockByHash(hash string, withTransactions bool) (*Block, error) {
	return rpc.getBlock("sl.getBlockByHash", withTransactions, hash, withTransactions)
}

// SlGetBlockByNumber returns information about a block by block number.
func (rpc *SlRPC) SlGetBlockByNumber(number int, withTransactions bool) (*Block, error) {
	return rpc.getBlock("sl.getBlockByNumber", withTransactions, IntToHex(number), withTransactions)
}

func (rpc *SlRPC) getTransaction(method string, params ...interface{}) (*Transaction, error) {
	transaction := new(Transaction)

	err := rpc.call(method, transaction, params...)
	return transaction, err
}

// SlGetTransactionByHash returns the information about a transaction requested by transaction hash.
func (rpc *SlRPC) SlGetTransactionByHash(hash string) (*Transaction, error) {
	return rpc.getTransaction("sl.getTransactionByHash", hash)
}

// SlGetTransactionByBlockHashAndIndex returns information about a transaction by block hash and transaction index position.
func (rpc *SlRPC) SlGetTransactionByBlockHashAndIndex(blockHash string, transactionIndex int) (*Transaction, error) {
	return rpc.getTransaction("sl.getTransactionByBlockHashAndIndex", blockHash, IntToHex(transactionIndex))
}

// SlGetTransactionByBlockNumberAndIndex returns information about a transaction by block number and transaction index position.
func (rpc *SlRPC) SlGetTransactionByBlockNumberAndIndex(blockNumber, transactionIndex int) (*Transaction, error) {
	return rpc.getTransaction("sl.getTransactionByBlockNumberAndIndex", IntToHex(blockNumber), IntToHex(transactionIndex))
}

// SlGetTransactionReceipt returns the receipt of a transaction by transaction hash.
// Note That the receipt is not available for pending transactions.
func (rpc *SlRPC) SlGetTransactionReceipt(hash string) (*TransactionReceipt, error) {
	transactionReceipt := new(TransactionReceipt)

	err := rpc.call("sl.getTransactionReceipt", transactionReceipt, hash)
	if err != nil {
		return nil, err
	}

	return transactionReceipt, nil
}

// Eth1 returns 1 ethereum value (10^18 wei)
func (rpc *SlRPC) Eth1() *big.Int {
	return Eth1()
}

// Eth1 returns 1 ethereum value (10^18 wei)
func Eth1() *big.Int {
	return big.NewInt(1000000000000000000)
}
