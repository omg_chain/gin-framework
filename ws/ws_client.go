package ws

import (
	"log"
	"net/url"
	"os"
	"os/signal"
	"time"

	"fmt"
	"github.com/gorilla/websocket"
)

var (
	wsConn *websocket.Conn
)

func Init() {
	addr := ":9999"
	//addr := "127.0.0.1:9999"
	u := url.URL{Scheme: "ws", Host: addr, Path: "/ws"}

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	wsConn = c
}


func NewClient() {
	c := wsConn
	if c == nil {
		log.Fatal("ws_con nil")
	}

	defer c.Close()

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	//done := make(chan struct{})
	done := make(chan bool)

	//ticker := time.NewTicker(time.Millisecond * 1)
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-done:
			log.Printf("receive done signal...")
			return
		//case t := <-ticker.C:
		case <-ticker.C:
			// todo writeMessage
			data, err := writeMessage()
			if err != nil {
				log.Println("send deal msg error:", err)
				return
			}
			sendMsg := fmt.Sprintf("time: %v, data as following:\n%v", time.Now(), data)
			err = c.WriteMessage(websocket.TextMessage, []byte(data))
			if err != nil {
				log.Println("send write msg error:", err)
				return
			}
			log.Printf("%s", sendMsg)

			//sendMsg := fmt.Sprintf("eat dark at %v", time.Now())
			//err := c.WriteMessage(websocket.TextMessage, []byte(t.String()))
			//if err != nil {
			//	log.Println("send msg error:", err)
			//	//logs.Println("write:", err)
			//	return
			//}
			//log.Printf("send: %s", sendMsg)

		case <-interrupt:
			log.Println("interrupt")

			// Cleanly close the connection by sending a close message and then
			// waiting (with timeout) for the server to close the connection.
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, "interrupt"))
			if err != nil {
				log.Println("write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return
		}
	}

}
