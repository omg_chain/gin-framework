package json_rpc

import (
	"log"
	"testing"
)

var rpcUrl = "http://127.0.0.1:8545"

// temp test
func TestGoodWeb3ClientVersion(t *testing.T) {
	client := New(rpcUrl)
	web3ClientVersion, err := client.GoodWeb3ClientVersion()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Web3ClientVersion is %v", web3ClientVersion)

}

func TestWeb3ClientVersion(t *testing.T) {
	client := New(rpcUrl)
	web3ClientVersion, err := client.Web3ClientVersion()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Web3ClientVersion is %v", web3ClientVersion)

}

func TestWeb3Sha3(t *testing.T) {
	client := New(rpcUrl)
	str := "Seele"
	sha3Val, err := client.Web3Sha3([]byte(str))

	if err != nil {
		log.Fatal(err)
	}
	log.Printf("SHA3-256 for %v is %v", str, sha3Val)

}

func TestNetVersion(t *testing.T) {
	client := New(rpcUrl)
	netVersion, err := client.NetVersion()

	if err != nil {
		log.Fatal(err)
	}
	log.Printf("netVersion is %v", netVersion)

}

func TestNetNetListening(t *testing.T) {
	client := New(rpcUrl)
	linstenState, err := client.NetListening()

	if err != nil {
		log.Fatal(err)
	}
	log.Printf("linstenState is %v", linstenState)

}
func TestNetPeerCount(t *testing.T) {
	client := New(rpcUrl)
	netPeerCount, err := client.NetPeerCount()

	if err != nil {
		log.Fatal(err)
	}
	log.Printf("netPeerCount is %v", netPeerCount)

}
func TestSlProtocolVersion(t *testing.T) {
	client := New(rpcUrl)
	slProtocolVersion, err := client.SlProtocolVersion()

	if err != nil {
		log.Fatal(err)
	}
	log.Printf("slProtocolVersion is %v", slProtocolVersion)

}
